import * as React from 'react';

export interface IHelloProps {
}

export default function Hello (props: IHelloProps) {
  return (
    <div>
        <h1 className="text-red-600">hello</h1>
    </div>
  );
}
